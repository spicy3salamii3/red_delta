﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TimerCollider23 : MonoBehaviour
{
    // Start is called before the first frame update

    void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            Time.timeScale = 1f;
            Debug.Log("Timer has started.");
            StartCoroutine("Timer");
        }
    }

    IEnumerator Timer()
    {
        yield return new WaitForSeconds(0.008f);
    }
}
