﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pause : MonoBehaviour
{
    public GameObject pMENU;

    public float counter = 0;


    void Start()
    {
        Time.timeScale = 1f;
        pMENU.SetActive(false);
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            
            counter += 1;
            pMENU.SetActive(true);

            Time.timeScale = 0.5f;

        }

        if (counter == 2)
        {
            Time.timeScale = 1f;
            counter = 0;
            pMENU.SetActive(false);
        }
    }
}
