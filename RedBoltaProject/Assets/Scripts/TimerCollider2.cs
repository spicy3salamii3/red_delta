﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TimerCollider2 : MonoBehaviour
{

    private bool timerSTARTED;

    int time = 0;

    void Start()
    {
        timerSTARTED = false;
    }

    void OnGUI()
    {
        if (timerSTARTED)
        {
            GUI.Label(new Rect(10, 250, 200, 50), "Time Elapsed: " + time);
        }  
        
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            Time.timeScale = 1f;
            Debug.Log("Timer has started.");
            timerSTARTED = true;
            StartCoroutine("Timer");
        }
    }

    IEnumerator Timer()
    {
        while (true)
        {
            yield return new WaitForSeconds(1f);
            time += 1;
        }
        
    }
}

