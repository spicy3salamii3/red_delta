﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class FreeDriveButton : MonoBehaviour
{
    
    public string loadlevel;

    public void FreeDrive()
    {
        SceneManager.LoadScene(loadlevel);
    }
}
