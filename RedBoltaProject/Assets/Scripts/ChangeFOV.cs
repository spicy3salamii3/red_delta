﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChangeFOV : MonoBehaviour
{
    public float counter = 0;

    public GameObject FOV1;
    public GameObject FOV2;
    public GameObject FOV3;

    AudioListener FOV1AudioLis;
    AudioListener FOV2AudioLis;
    AudioListener FOV3AudioLis;

    void Start()
    {
        FOV1AudioLis = FOV1.GetComponent<AudioListener>();
        FOV2AudioLis = FOV2.GetComponent<AudioListener>();
        FOV3AudioLis = FOV3.GetComponent<AudioListener>();

        FOV1.SetActive(false);
        FOV2.SetActive(true);
        FOV3.SetActive(false);

    }


    // Update is called once per frame
    void Update()
    {
        switchCamera();
    }

    void switchCamera()
    {
        if (Input.GetKeyDown(KeyCode.Tab))
        {
            counter += 1;
            checkCOUNTER();

            if (counter > 3)
            {
                counter = 1;
                checkCOUNTER();
            }

        }
        
    }

    void checkCOUNTER()
    {
        if (counter == 1)
        {
            FOV1.SetActive(false);
            FOV2.SetActive(true);
            FOV3.SetActive(false);
            FOV2AudioLis.enabled = true;
            FOV1AudioLis.enabled = false;
            FOV3AudioLis.enabled = false;
        }

        if (counter == 2)
        {
            FOV1.SetActive(true);
            FOV2.SetActive(false);
            FOV3.SetActive(false);
            FOV2AudioLis.enabled = false;
            FOV1AudioLis.enabled = true;
            FOV3AudioLis.enabled = false;
        }

        if (counter == 3)
        {
            FOV1.SetActive(false);
            FOV2.SetActive(false);
            FOV3.SetActive(true);
            FOV2AudioLis.enabled = false;
            FOV1AudioLis.enabled = false;
            FOV3AudioLis.enabled = true;
        }    
    }
}
